package trado

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

/*

live.tradovateapi.com for Live only functionality.
demo.tradovateapi.com for simulation engine.
md.tradovateapi.com and for a market data feed.

*/

var HTTPClient *http.Client

type TradovateClient struct {
	BaseUrl  string `json:"base_url"`
	LiveUrl  string `json:"live_url"`
	PaperUrl string `json:"paper_url"`
	DataUrl  string `json:"data_url"`

	TokenRequestUrl     string `json:"token_request_url"`      // POST
	OauthTokenUrl       string `json:"oath_token_url"`         // POST
	RenewAccessTokenUrl string `json:"renew_access_token_url"` // GET

	PositionUrl  string `json:"position"`
	PositionsUrl string `json:"positions"`

	CancelOrderUrl       string `json:"cancel_order"`
	PlaceOrderUrl        string `json:"place_order"`
	PlaceOSOOrderUrl     string `json:"place_oso_order"`
	PlaceOCOOrderUrl     string `json:"place_oco_order"`
	LiquidatePositionUrl string `json:"liquidate_position"`
	OrderStatusUrl       string `json:"order_status"`
	OrdersStatusUrl      string `json:"orders_status"`

	AccountBalanceURl string `json:"account_balance"`
}

var TClient TradovateClient

func init() {

	timeout := 25 * time.Second
	HTTPClient = &http.Client{
		Timeout: timeout,
	}

	TClient = TradovateClient{
		BaseUrl:  "",
		LiveUrl:  "https://live.tradovateapi.com/v1/",
		PaperUrl: "https://demo.tradovateapi.com/v1/",
		DataUrl:  "https://md.tradovateapi.com/v1/",

		PositionUrl:  "position/item",
		PositionsUrl: "position/items",

		CancelOrderUrl:       "order/cancelorder",
		PlaceOrderUrl:        "order/placeorder",
		PlaceOSOOrderUrl:     "order/placeoso",
		PlaceOCOOrderUrl:     "order/placeoco",
		LiquidatePositionUrl: "order/liquidateposition",
		OrderStatusUrl:       "order/item",
		OrdersStatusUrl:      "order/items",
	}
}

func (t TradovateClient) UsePaperUrl() {
	t.BaseUrl = t.PaperUrl
}

func (t TradovateClient) UseLiveUrl() {
	t.BaseUrl = t.LiveUrl
}

func newRequest(method, url string, body io.Reader) (*http.Request, error) {
	var req *http.Request
	var err error

	if body == nil {
		req, err = http.NewRequest(method, url, nil)
	} else {
		req, err = http.NewRequest(method, url, body)
	}

	if err != nil {
		return req, err
	}

	authToken := os.Getenv("AUTH_TOKEN")
	if body == nil {
		req.Header.Set("Accept", "application/json")
	} else {
		req.Header.Set("Accept", "*/*")
		req.Header.Set("Content-Type", "application/json")
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", authToken))

	return req, nil
}
