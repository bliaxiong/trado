package trado

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/bliaxiong/trado/util"
)

func (t TradovateClient) LiquidatePosition(accountId, contractId int64) (*http.Response, error) {
	url := fmt.Sprintf("%s%s", t.BaseUrl, t.LiquidatePositionUrl)

	cancelOrder := LiquidatePosition{AccountID: accountId, ContractID: contractId, Admin: true}
	data, err := json.Marshal(cancelOrder)

	if err != nil {
		return nil, err
	}

	payload := bytes.NewReader(data)

	req, err := newRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	res, err := HTTPClient.Do(req)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (t TradovateClient) FetchPositionReq(id int32) (*http.Response, error) {
	v := url.Values{}
	v.Set("id", util.IntToStr(int64(id)))

	url := fmt.Sprintf("%s%s%s", t.BaseUrl, t.PositionUrl, v.Encode())

	req, err := newRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	res, err := HTTPClient.Do(req)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (t TradovateClient) FetchPositions(ids []int64) (*http.Response, error) {
	v := url.Values{}
	positionIds := PositionIds{Id: ids}
	data, err := json.Marshal(positionIds)

	if err != nil {
		return nil, err
	}

	v.Set("ids", string(data))

	url := fmt.Sprintf("%s%s%s", t.BaseUrl, t.PositionsUrl, v.Encode())

	req, err := newRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	res, err := HTTPClient.Do(req)

	if err != nil {
		return nil, err
	}

	return res, nil
}
