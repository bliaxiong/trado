package trado

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/bliaxiong/trado/util"

	"github.com/goccy/go-json"
)

func (t TradovateClient) CancelOrderReq(orderId int32) (*http.Response, error) {
	url := fmt.Sprintf("%s%s", t.BaseUrl, t.PositionUrl)

	cancelOrder := CancelOrder{OrderID: orderId, IsAutomated: true}
	data, err := json.Marshal(cancelOrder)

	if err != nil {
		return nil, err
	}

	payload := bytes.NewReader(data)

	req, err := newRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	res, err := HTTPClient.Do(req)

	if err != nil {
		return nil, err
	}

	return res, nil
}

// Should use the OrderStatus{} to unmarshall data into
func (t TradovateClient) FetchOrderStatus(orderId int32) (*http.Response, error) {
	v := url.Values{}
	v.Set("id", util.IntToStr(int64(orderId)))

	url := fmt.Sprintf("%s%s%s", t.BaseUrl, t.OrderStatusUrl, v.Encode())

	req, err := newRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	res, err := HTTPClient.Do(req)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (t TradovateClient) InteruptOrderStrategy(orderStrategyId int64) (*http.Response, error) {
	url := fmt.Sprintf("%s%s", t.BaseUrl, t.LiquidatePositionUrl)

	cancelOrder := InteruptOrderStrategy{OrderStrategyID: orderStrategyId}
	data, err := json.Marshal(cancelOrder)

	if err != nil {
		return nil, err
	}

	payload := bytes.NewReader(data)

	req, err := newRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	res, err := HTTPClient.Do(req)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (t TradovateClient) PlaceOrder(order Order) (*http.Response, error) {
	url := fmt.Sprintf("%s%s", t.BaseUrl, t.PositionUrl)

	data, err := json.Marshal(order)

	if err != nil {
		return nil, err
	}

	payload := bytes.NewReader(data)

	req, err := newRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	res, err := HTTPClient.Do(req)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (t TradovateClient) PlaceOSOOrder(osoOrder OSOOrder) (*http.Response, error) {
	url := fmt.Sprintf("%s%s", t.BaseUrl, t.PositionUrl)

	data, err := json.Marshal(osoOrder)

	if err != nil {
		return nil, err
	}

	payload := bytes.NewReader(data)

	req, err := newRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	res, err := HTTPClient.Do(req)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (t TradovateClient) PlaceOCOOrder(ocoOrder OCOOrder) (*http.Response, error) {
	url := fmt.Sprintf("%s%s", t.BaseUrl, t.PositionUrl)

	data, err := json.Marshal(ocoOrder)

	if err != nil {
		return nil, err
	}

	payload := bytes.NewReader(data)

	req, err := newRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	res, err := HTTPClient.Do(req)

	if err != nil {
		return nil, err
	}

	return res, nil
}
