package util

import (
	"fmt"
	"math"
	"strconv"
)

// FormatFloat - formats a float to 2 decimal places
func FormatFloat(myFloat float64) float64 {
	return math.Round(myFloat*100) / 100
}

// FormatFloat - formats a float to 2 decimal places and rounds down
func RoundDownFloat(myFloat float64) float64 {
	return math.Floor(myFloat*100) / 100
}

// Int64ToStr works
func IntToStr(num int64) string {
	s := strconv.FormatInt(num, 10)
	return s
	// return strconv.Itoa(num)
}

// StrToInt64 works
func StrToInt64(num string) int64 {
	i, _ := strconv.ParseInt(num, 10, 64)
	return i
}

// StrToInt works
func StrToInt(num string) int64 {
	i, _ := strconv.ParseInt(num, 10, 32)
	return i
}

// FloatToStr works
func FloatToStr(num float64) string {
	return fmt.Sprintf("%f", num)
}

// StrToFloat works
func StrToFloat(num string) float64 {
	float, _ := strconv.ParseFloat(num, 64)
	return float
}

// StringToInt works
func StringToInt(str string) int {
	var count int
	if str == "" {
		count = 0
	} else {
		var err error
		count, err = strconv.Atoi(str)
		if err != nil {
			// TODO
		}
	}
	return count
}

func HTTPSuccess(code int) bool {
	return code > 199 && code < 300
}
