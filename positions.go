package trado

import "time"

type PositionIds struct {
	Id []int64 `json:"id"`
}

type TradeDate struct {
	Year  int `json:"year"`
	Month int `json:"month"`
	Day   int `json:"day"`
}

type Position struct {
	ID          int32     `json:"id"`
	AccountID   int32     `json:"accountId"`
	ContractID  int32     `json:"contractId"`
	Timestamp   time.Time `json:"timestamp"`
	TradeDate   TradeDate `json:"tradeDate"`
	NetPos      float32   `json:"netPos"`
	NetPrice    float32   `json:"netPrice"`
	Bought      float32   `json:"bought"`
	BoughtValue float32   `json:"boughtValue"`
	Sold        float32   `json:"sold"`
	SoldValue   float32   `json:"soldValue"`
	PrevPos     float32   `json:"prevPos"`
	PrevPrice   float32   `json:"prevPrice"`
}

type Positions struct {
	Positions []Position
}

// Response to LiquidatePosition()
type LiquidatePosition struct {
	AccountID   int64  `json:"accountId"`  // Account number
	ContractID  int64  `json:"contractId"` // ???
	Admin       bool   `json:"admin"`      // always true
	CustomTag50 string `json:"customTag50"`
}
