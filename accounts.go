package trado

type AccountBalance struct {
	ErrorText         string  `json:"errorText"`
	TotalCashValue    float32 `json:"totalCashValue"`
	TotalPnL          float32 `json:"totalPnL"`
	InitialMargin     float32 `json:"initialMargin"`
	MaintenanceMargin float32 `json:"maintenanceMargin"`
	NetLiq            float32 `json:"netLiq"`
	OpenPnL           float32 `json:"openPnL"`
	RealizedPnL       float32 `json:"realizedPnL"`
	WeekRealizedPnL   float32 `json:"weekRealizedPnL"`
}
