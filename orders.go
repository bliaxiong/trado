package trado

import "time"

type OrderID struct {
	Id int32 `json:"id"`
}

type OrderIDs struct {
	Ids []OrderID `json:"ids"`
}

type CancelOrder struct {
	OrderID        int32     `json:"orderId"` // Order ID
	ClOrdID        string    `json:"clOrdId"`
	ActivationTime time.Time `json:"activationTime"`
	CustomTag50    string    `json:"customTag50"`
	IsAutomated    bool      `json:"isAutomated"`
}

type CancelOrderResponse struct {
	FailureReason string `json:"failureReason"` // AccountClosed, AdvancedTrailingStopUnsupported, AnotherCommandPending, BackMonthProhibited, ExecutionProviderNotConfigured, ExecutionProviderUnavailable, InvalidContract, InvalidPrice, LiquidationOnly, LiquidationOnlyBeforeExpiration, MaxOrderQtyIsNotSpecified, MaxOrderQtyLimitReached, MaxPosLimitMisconfigured, MaxPosLimitReached, MaxTotalPosLimitReached, MultipleAccountPlanRequired, NoQuote, NotEnoughLiquidity, OtherExecutionRelated, ParentRejected, RiskCheckTimeout, SessionClosed, Success, TooLate, TradingLocked, TrailingStopNonOrderQtyModify, Unauthorized, UnknownReason, Unsupported
	FailureText   string `json:"failureText"`
	CommandID     int32  `json:"commandId"`
}

// Regular order to POST
type Order struct {
	AccountSpec    string    `json:"accountSpec"` // User name
	AccountID      int32     `json:"accountId"`   // Account Number
	ClOrdID        string    `json:"clOrdId"`
	Action         string    `json:"action"` // Buy|Sell
	Symbol         string    `json:"symbol"` // Ticker
	OrderQty       int       `json:"orderQty"`
	OrderType      string    `json:"orderType"` // "Limit" "MIT" "Market" "QTS" "Stop" "StopLimit" "TrailingStop" "TrailingStopLimit"
	Price          float32   `json:"price"`
	StopPrice      float32   `json:"stopPrice"`
	MaxShow        int       `json:"maxShow"`
	PegDifference  int       `json:"pegDifference"`
	TimeInForce    string    `json:"timeInForce"` // "Day" "FOK" "GTC" "GTD" "IOC"
	ExpireTime     time.Time `json:"expireTime"`
	Text           string    `json:"text"`
	ActivationTime time.Time `json:"activationTime"`
	CustomTag50    string    `json:"customTag50"`
	IsAutomated    bool      `json:"isAutomated"`
}

type OrderReponse struct {
	FailureReason string `json:"failureReason"` // AccountClosed, AdvancedTrailingStopUnsupported, AnotherCommandPending, BackMonthProhibited, ExecutionProviderNotConfigured, ExecutionProviderUnavailable, InvalidContract, InvalidPrice, LiquidationOnly, LiquidationOnlyBeforeExpiration, MaxOrderQtyIsNotSpecified, MaxOrderQtyLimitReached, MaxPosLimitMisconfigured, MaxPosLimitReached, MaxTotalPosLimitReached, MultipleAccountPlanRequired, NoQuote, NotEnoughLiquidity, OtherExecutionRelated, ParentRejected, RiskCheckTimeout, SessionClosed, Success, TooLate, TradingLocked, TrailingStopNonOrderQtyModify, Unauthorized, UnknownReason, Unsupported
	FailureText   string `json:"failureText"`
	OrderID       int    `json:"orderId"`
}

// OCO - places a STOP or LIMIT order on an existing position
type OCOOrder struct {
	AccountSpec    string    `json:"accountSpec"` // User name
	AccountID      int32     `json:"accountId"`   // Account number
	ClOrdID        string    `json:"clOrdId"`
	Action         string    `json:"action"` // Buy|Sell
	Symbol         string    `json:"symbol"`
	OrderQty       int       `json:"orderQty"`
	OrderType      string    `json:"orderType"`
	Price          float32   `json:"price"`
	StopPrice      float32   `json:"stopPrice"`
	MaxShow        int       `json:"maxShow"`
	PegDifference  int       `json:"pegDifference"`
	TimeInForce    string    `json:"timeInForce"` // "Day" "FOK" "GTC" "GTD" "IOC"
	ExpireTime     time.Time `json:"expireTime"`
	Text           string    `json:"text"`
	ActivationTime time.Time `json:"activationTime"`
	CustomTag50    string    `json:"customTag50"`
	IsAutomated    bool      `json:"isAutomated"`
	Other          OCOOther  `json:"other"`
}

type OCOOther struct {
	Action        string    `json:"action"`
	ClOrdID       string    `json:"clOrdId"`
	OrderType     string    `json:"orderType"`
	Price         float32   `json:"price"`
	StopPrice     float32   `json:"stopPrice"`
	MaxShow       int       `json:"maxShow"`
	PegDifference int       `json:"pegDifference"`
	TimeInForce   string    `json:"timeInForce"` // "Day" "FOK" "GTC" "GTD" "IOC"
	ExpireTime    time.Time `json:"expireTime"`
	Text          string    `json:"text"`
}

type OCOOrderResponse struct {
	FailureReason string `json:"failureReason"` // AccountClosed, AdvancedTrailingStopUnsupported, AnotherCommandPending, BackMonthProhibited, ExecutionProviderNotConfigured, ExecutionProviderUnavailable, InvalidContract, InvalidPrice, LiquidationOnly, LiquidationOnlyBeforeExpiration, MaxOrderQtyIsNotSpecified, MaxOrderQtyLimitReached, MaxPosLimitMisconfigured, MaxPosLimitReached, MaxTotalPosLimitReached, MultipleAccountPlanRequired, NoQuote, NotEnoughLiquidity, OtherExecutionRelated, ParentRejected, RiskCheckTimeout, SessionClosed, Success, TooLate, TradingLocked, TrailingStopNonOrderQtyModify, Unauthorized, UnknownReason, Unsupported
	FailureText   string `json:"failureText"`
	OrderID       int    `json:"orderId"`
	OcoID         int    `json:"ocoId"`
}

// This is similar to OTOCO???
type OSOBracket struct {
	Action        string    `json:"action"` // Sell|Buy
	ClOrdID       string    `json:"clOrdId"`
	OrderType     string    `json:"orderType"` // Limit, Stop
	Price         float32   `json:"price"`
	StopPrice     float32   `json:"stopPrice"`
	MaxShow       int       `json:"maxShow"`
	PegDifference int       `json:"pegDifference"`
	TimeInForce   string    `json:"timeInForce"` // "Day" "FOK" "GTC" "GTD" "IOC"
	ExpireTime    time.Time `json:"expireTime"`
	Text          string    `json:"text"`
}
type OSOOrder struct {
	AccountSpec    string     `json:"accountSpec"` // User name
	AccountID      int32      `json:"accountId"`   // Account ID
	ClOrdID        string     `json:"clOrdId"`
	Action         string     `json:"action"` // Sell|Buy
	Symbol         string     `json:"symbol"`
	OrderQty       int        `json:"orderQty"`
	OrderType      string     `json:"orderType"` // Limit
	Price          float32    `json:"price"`
	StopPrice      float32    `json:"stopPrice"`
	MaxShow        int        `json:"maxShow"`
	PegDifference  int        `json:"pegDifference"`
	TimeInForce    string     `json:"timeInForce"` // "Day" "FOK" "GTC" "GTD" "IOC"
	ExpireTime     time.Time  `json:"expireTime"`
	Text           string     `json:"text"`
	ActivationTime time.Time  `json:"activationTime"`
	CustomTag50    string     `json:"customTag50"`
	IsAutomated    bool       `json:"isAutomated"`
	Bracket1       OSOBracket `json:"bracket1"`
	Bracket2       OSOBracket `json:"bracket2"`
}

type OSOOrderResponse struct {
	FailureReason string `json:"failureReason"` // AccountClosed, AdvancedTrailingStopUnsupported, AnotherCommandPending, BackMonthProhibited, ExecutionProviderNotConfigured, ExecutionProviderUnavailable, InvalidContract, InvalidPrice, LiquidationOnly, LiquidationOnlyBeforeExpiration, MaxOrderQtyIsNotSpecified, MaxOrderQtyLimitReached, MaxPosLimitMisconfigured, MaxPosLimitReached, MaxTotalPosLimitReached, MultipleAccountPlanRequired, NoQuote, NotEnoughLiquidity, OtherExecutionRelated, ParentRejected, RiskCheckTimeout, SessionClosed, Success, TooLate, TradingLocked, TrailingStopNonOrderQtyModify, Unauthorized, UnknownReason, Unsupported
	FailureText   string `json:"failureText"`
	OrderID       int    `json:"orderId"`
	Oso1ID        int    `json:"oso1Id"`
	Oso2ID        int    `json:"oso2Id"`
}

// Response from server
// Response to FetchOrderStatus()
type OrderStatus struct {
	ID                  int32     `json:"id"`
	AccountID           int32     `json:"accountId"` // Account number
	ContractID          int32     `json:"contractId"`
	SpreadDefinitionID  int32     `json:"spreadDefinitionId"`
	Timestamp           time.Time `json:"timestamp"`
	Action              string    `json:"action"`    // Buy, Sell
	OrdStatus           string    `json:"ordStatus"` // Canceled, Completed, Expired, Filled, PendingCancel, PendingNew, PendingReplace, Rejected, Suspended, Unknown, Working
	ExecutionProviderID int32     `json:"executionProviderId"`
	OcoID               int32     `json:"ocoId"`
	ParentID            int32     `json:"parentId"`
	LinkedID            int32     `json:"linkedId"`
	Admin               bool      `json:"admin"`
}

type OrderStatuses struct {
	Orders []OrderStatus `json:"orders_statuses"`
}

type ModifyOrder struct {
	OrderID        int32     `json:"orderId"` // existing order ID
	ClOrdID        string    `json:"clOrdId"`
	OrderQty       int       `json:"orderQty"`
	OrderType      string    `json:"orderType"` // Limit, Stop, etc
	Price          float32   `json:"price"`
	StopPrice      float32   `json:"stopPrice"`
	MaxShow        int       `json:"maxShow"`
	PegDifference  int       `json:"pegDifference"`
	TimeInForce    string    `json:"timeInForce"` // "Day" "FOK" "GTC" "GTD" "IOC"
	ExpireTime     time.Time `json:"expireTime"`
	Text           string    `json:"text"`
	ActivationTime time.Time `json:"activationTime"`
	CustomTag50    string    `json:"customTag50"`
	IsAutomated    bool      `json:"isAutomated"` // always true
}

type InteruptOrderStrategy struct {
	OrderStrategyID int64 `json:"orderStrategyId"`
}
